
--- create db
CREATE DATABASE emp_test
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	

DROP TABLE IF EXISTS salaries;
DROP TABLE IF EXISTS titles;
DROP TABLE IF EXISTS dept_emp;
DROP TABLE IF EXISTS dept_manager;
DROP TABLE IF EXISTS departments;
DROP TABLE IF EXISTS employees;

-- create tables

CREATE TABLE IF NOT EXISTS departments(
	id SERIAL PRIMARY KEY,
	dept_no VARCHAR(4) NOT NULL UNIQUE,
	dept_name VARCHAR(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS employees(
	id SERIAL PRIMARY KEY,
	emp_no INT NOT NULL UNIQUE,
	birth_date DATE NOT NULL,
	first_name VARCHAR(30) NOT NULL,
	last_name VARCHAR(30) NOT NULL,
	gender CHAR(1) NOT NULL,
	hire_date DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS titles(
	id SERIAL PRIMARY KEY,
	emp_no INT NOT NULL, 
	title VARCHAR(30) NOT NULL,
	from_date DATE NOT NULL,
	to_date DATE NOT NULL

);

CREATE TABLE IF NOT EXISTS dept_emp(
	id SERIAL PRIMARY KEY,
	emp_no INT NOT NULL,
	dept_no VARCHAR(4) NOT NULL,
	from_date DATE NOT NULL,
	to_date DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS dept_manager(
	id SERIAL PRIMARY KEY,
	dept_no VARCHAR(4) NOT NULL,
	emp_no INT NOT NULL,
	from_date DATE NOT NULL,
	to_date DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS salaries(
	id SERIAL PRIMARY KEY,
	emp_no INT NOT NULL,
	salary NUMERIC (7) NOT NULL,
	from_date DATE NOT NULL,
	to_date DATE NOT NULL
);


-- load data
COPY departments(dept_no,dept_name) 
FROM '/Users/joef/tmp/sql_hw/data/departments.csv'
DELIMITER ','
CSV HEADER;

COPY employees(emp_no,birth_date,first_name,last_name,gender,hire_date) 
FROM '/Users/joef/tmp/sql_hw/data/employees.csv'
DELIMITER ','
CSV HEADER;

COPY titles(emp_no,title,from_date,to_date) 
FROM '/Users/joef/tmp/sql_hw/data/titles.csv'
DELIMITER ','
CSV HEADER;

COPY dept_emp(emp_no,dept_no,from_date,to_date) 
FROM '/Users/joef/tmp/sql_hw/data/dept_emp.csv'
DELIMITER ','
CSV HEADER;

COPY dept_manager(dept_no,emp_no,from_date,to_date) 
FROM '/Users/joef/tmp/sql_hw/data/dept_manager.csv'
DELIMITER ','
CSV HEADER;

COPY salaries(emp_no,salary,from_date,to_date) 
FROM '/Users/joef/tmp/sql_hw/data/salaries.csv'
DELIMITER ','
CSV HEADER;


-- set up constraints afer data load
-- add title table fk to employees
ALTER TABLE titles
ADD CONSTRAINT TITLES_EMP_NO_FKEY
FOREIGN KEY (emp_no)
REFERENCES employees(emp_no);

-- add dept_emp fk to employees
ALTER TABLE dept_emp
ADD CONSTRAINT DEPT_EMP_EMP_NO_FKEY
FOREIGN KEY (emp_no)
REFERENCES employees(emp_no);

-- add dept_emp fk to departments
ALTER TABLE dept_emp
ADD CONSTRAINT DEPT_EMPT_DEPT_NO_FKEY
FOREIGN KEY (dept_no)
REFERENCES departments(dept_no);

-- add dept_manager fk to employees
ALTER TABLE dept_manager
ADD CONSTRAINT DEPT_MANAGER_EMP_NO_FKEY
FOREIGN KEY (emp_no)
REFERENCES employees(emp_no);

-- add dept_manager fk to departments
ALTER TABLE dept_manager
ADD CONSTRAINT DEPT_MANAGER_DEPT_NO_FKEY
FOREIGN KEY (dept_no)
REFERENCES departments(dept_no);

-- add salaries fk to employees
ALTER TABLE salaries
ADD CONSTRAINT EMP_NO_FKEY
FOREIGN KEY (emp_no)
REFERENCES employees(emp_no);


